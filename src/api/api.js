// make fetch requests and handle response 

import {endpoints} from './data.js';
console.log(endpoints.books);

const request=(url,options,callback)=> {
    
    fetch(url,options)
.then(res=>res.json())
.then(books=>callback(books))
.catch(err=>console.log(err.message));
}


function createOptions(method='get',data=false) {
    const options = {
        method,
        headers:{

             'X-Parse-Application-Id':'Ecf1CAwJtN3zBcPtRuORZfJf0IMkLKr3VUr9c6Pm',
             'X-Parse-REST-API-Key':'vvIADXNQq8puQ9n9MOALMUTSeikvPcJCXor9CF9p'
            }
    }
    if (data) {
       console.log(data);
       
       console.log(options);
     
       options.body=JSON.stringify(data)
   }

   return options;
}

function getAllBooks(callback) {
   
    return request(endpoints.books,createOptions(),callback)   
}

function getBookById(callback,id) {
    return request(endpoints.getOneBook(id),createOptions(),callback,id)   
}

export  {request,createOptions,getAllBooks,getBookById};