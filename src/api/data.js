// routes

export const endpoints = {
    books:'https://parseapi.back4app.com/classes/books',
    getOneBook:(id)=>`https://parseapi.back4app.com/classes/books/${id}`
}