import { Link } from "react-router-dom";
import styles from './Button.module.css';

function Button(props) {
    const {onClick,count,typeBtn}=props;
    if (typeBtn==='welcome') {
        
        return (
            <div className={styles.wrapBtns}>
            <Link className={styles.personalizeBtnWelcome} onClick={onClick} to='/personalization'>Personalize Your Account</Link>
            <Link className={styles.skipBtnWelcome} to='/greetings'>Skip</Link>
        </div>
        );
    } else if (typeBtn==='greetings') {
      return (
        <Link className={styles.finishBtn}  to='/library'>Finish</Link>
        );
    } else if (typeBtn==='personalization') {
        return (
        <div className={styles.wrapBtnsPersonalization}>
            <Link className={styles.personalizeBtnSubmit} onClick={onClick} to='/greetings'>Submit</Link>
            <Link className={styles.skipBtnPersonalization} to='/greetings'>Skip</Link>
        </div>
        );
    }
        

    if (count>=3) {
        return (
        <Link className={styles.letsStart} to='/welcome'>Lets Get Started</Link>
            );
    } else {
        return (
        <div className={styles.wrapBtns}>
            <Link className={styles.skipBtn} to='/welcome'>Skip</Link>
            <Link className={styles.nextBtn} onClick={onClick} to='/Illustration-2.png'>Next</Link>
        </div>
        );
    }
}

export default Button;