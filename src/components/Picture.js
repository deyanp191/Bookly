//this is for picture containers
import styles from './Picture.module.css';

function Picture(props) {  
    if (props.type==='bookCoverPlayer')    {
            return (  
                <img className={styles.bookCoverPlayer} src={`${props.img}`} alt={props.alt} />);
            } else  if (props) {
                return (
                <img className="picture" src={`${props.img}`} alt={props.alt} />);
            
        } else {
            return null;
        } 
}

export default Picture;