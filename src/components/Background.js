import styles from './Background.module.css';
import Picture from './Picture'

function Background(props) {
    if (props.img) {
    }
    if (props.typeBackground==='library'||props.typeBackground==='player') {
        return (
            <main className={styles.canvas}>
               {props.img?
                <Picture img={props.img}/>
                  :null} 
                 
               {props.children}
    
            </main>
        );
    } else {
        return (
        <main className={styles.canvas}>
           {props.img?
            <Picture img={props.img}/>
              :null} 
           <div className={styles.circleBackground}></div>
           <div className={styles.archBackground}></div>
           <div className={styles.archBackgroundSmall}></div>
           {props.children}

        </main>
    );
    }
    
}

export default Background;