import styles from './Carousel.module.css';

function Carousel(props) {
    let {count}=props;
       const dotElements=[];
        let styling=styles.dots;
        for (let i = 1; i <= 3; i++) {
            if (i===count) {
                styling=styles.dots;
    dotElements.push(styling)
            } else {
                styling=styles.dotsNotSelected;
                dotElements.push(styling)
            }
        }

    return (
        <ul className={styles.carousel}>
           { dotElements.map((x,i)=>{
           return <li key={i.toString()} className={x} ></li>
           })}
        </ul>
    );
}

export default Carousel;