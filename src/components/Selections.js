import styles from './Selections.module.css';
import {useState} from 'react';
import {  saveGanres } from '../api/books';

function Selections({selected}) {
    const buttons =['Art','Business','Biography','Comedy','Culture','Education','News','Philosophy','Psychology','Technology','Travel'];

    const [selections, setSelections]=useState([]);
    const [selectedButtons,setSelectedButtons]=useState(selected);
 
    const handleSelection=(event)=>{
        if (selections.some((x)=>x.ganre===event.target.value)) {
            return;
        } else {
            let styleSelected=styles.selected
            setSelections([...selections,{ganre:event.target.value,styleSelected}]);
            saveGanres(selections);//to locale storage;
            setSelectedButtons(!selectedButtons)
           
        }
        
    };
    
    const Button=({keys,value,onClick})=>{ 
        return (
            <input key={keys.toString}  className={selections.some((x)=>x.ganre===keys)?`${styles.selected}`:`${styles.inputPersonalization}`} type='button' value={value} onClick={onClick} />
        );
    }

    return (
        <div className={styles.buttons}>
                      {buttons.map((intrest)=>{
               return <Button key={intrest}  keys={intrest} value={intrest} onClick={handleSelection} />
            })}
            {selections.length>0?<p>{selections.length} topics Selected</p>:<p>0 topics Selected</p>}
        </div>
    );
}

export default Selections;