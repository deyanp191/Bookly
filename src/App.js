 
import { BrowserRouter,Routes,Route } from "react-router-dom";
import "./App.css";

import Greetings from "./pages/Greetings/Greetings";
import Intro from "./pages/Intro/Intro";
import Library from "./pages/Library/Library";
import Personalization from "./pages/Personalization/Personalization";
import Player from "./pages/Player/Player";
import Start from "./pages/Start/Start";
import Welcome from "./pages/Welcome/Welcome";

function App() {
  return (
    <div className="App">
      
<BrowserRouter>
<Routes>
  <Route  path='/:img'  element={<Start />}></Route>
  <Route exact path='/' element={<Intro />}/>
  <Route exact path='/welcome' element={<Welcome/>} />
  <Route exact path='/greetings' element={<Greetings />} />
  <Route exact path='/library' element={<Library/>} />
  <Route  path='/player/:id' element={<Player />} />
  <Route exact path='/personalization' element={<Personalization/>}/>
</Routes>
</BrowserRouter>
   
      
    </div>
  );
}

export default App;
