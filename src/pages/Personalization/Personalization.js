import Background from '../../components/Background';
import Button from '../../components/Button';
import Search from '../../components/Search';
import Selections from '../../components/Selections';
import styles from './Personalization.module.css';

function Personalization(props) {
    return (
       
        <Background>
            <div className={styles.selectionContainer}>
                <h1 className={styles.personalizationSuggestions}>Personalize Suggestion</h1>
                <p className={styles.textPersonalize}>Choose min.3 topics you like, we will give you more often that relate to it.</p>
           
            <Search typeSearch='placeholder' placeholder='Placeholder' > </Search>
            <Selections selected={false} />
            <Button typeBtn='personalization'/>
           
            </div>
        </Background>
        
    );
}
export default Personalization;