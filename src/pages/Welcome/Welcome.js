import Background from '../../components/Background';
import Button from '../../components/Button';
import styles from './Welcome.module.css';



function Welcome() {

    const WelcomeMessages=()=>{
        return <div>
             <div className={styles.welcome}>Welcome!</div>
          <div  className={styles.find}>Find what you are lookin for</div >
          <div className={styles.message} >By personalizie your account, we can help you to find what you like.</div >
        </div>
    }

    return (

        <div className={styles.welcome}>
        <Background >
            <WelcomeMessages />
            <Button typeBtn='welcome' />

        </Background>
        </div>
    );
}

export default Welcome;