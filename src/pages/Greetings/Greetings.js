import Background from '../../components/Background';
import Button from '../../components/Button';
import Picture from '../../components/Picture';
import styles from './Greetings.module.css';


function Greetings() {
        
    return (
        <div className={styles.greetings}>
            <Background >
            <h1>You are ready to go!</h1> 
            <h2>Congratulations, any interesting topicts will be shorrtly in your hands.</h2>  
            <Picture img='Illustration.png'/>
            <Button typeBtn='greetings' ></Button>
            </Background>
        </div>
    );
}

export default Greetings;