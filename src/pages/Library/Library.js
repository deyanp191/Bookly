
import {getAllBooks}from '../../api/api.js';
import Background from '../../components/Background';
import Nav from '../../components/Nav';
import styles from './Library.module.css';
import {useEffect, useState} from 'react';
import Search from '../../components/Search';
import Card from './Card';
import HomeInteractor from '../../components/HomeInteractor.js';

function Library(props) {
    const [booksData, setBooks] =useState(null);
    const [booksAll,setBooksAll]= useState(null);
    const [searchedBooks,setSearchedBooks]=useState(null);
    const [error,setError]=useState(null)

useEffect(()=>{
    getAllBooks(setBooks);
},[])

useEffect(()=>{
    if (booksData) {
        const {results}=booksData;
        setBooksAll(results);
    }
},[booksData])
 
const handleSearch=(event)=>{
    let filtered=booksAll.filter(book=>book.title.includes(event.target.value));
    setSearchedBooks(filtered)
}
 
if (searchedBooks) {
      return (
        <div className='libraty'>
            <Background typeBackground='library'>
            <Nav />
            <Search onSearch={handleSearch} />
            {searchedBooks ?<ul className={styles.cardDisplay}>{searchedBooks.map(books=> <Card src={books.picture.url} keys={books.objectId} title={books.title} author={books.author} />)}</ul>:console.log('no books')}
            <p>{error&&error.message}</p>
            <HomeInteractor />
            </Background>
        </div>
    );
} else  {
    return (
        <div className='libraty'>
            <Background typeBackground='library'>
            <Nav />
            <Search onSearch={handleSearch} />
            {booksAll ?<ul className={styles.cardDisplay}>{booksAll.map(books=> <Card src={books.picture.url} keys={books.objectId} title={books.title} author={books.author} />)}</ul>:null}
            <HomeInteractor />
            </Background>
        </div>
    );
}
  
}
export default Library;