import { Link } from 'react-router-dom';
import styles from './Card.module.css';


function Card(props) {
        if (props) {
                    return  <li key={props.keys} className={styles.card} ><Link to={`/player/${encodeURI(props.keys)}`}> 
                        <img className={styles.cardPic} src={props.src } alt='img'/>
                        <div className={styles.authorAndTitle}>
                            <div className={styles.title}>{props.title}</div>
                            <div className={styles.author}>{props.author}</div>
                        </div>
                    </Link>
                    </li>
        } else {
            return null;
        }
    
}

export default Card;