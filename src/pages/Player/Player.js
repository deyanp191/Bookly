import { useParams } from 'react-router-dom';
import {useState,useEffect} from 'react';
import Background from '../../components/Background';
import HomeInteractor from '../../components/HomeInteractor';
import { getBookById } from '../../api/api';
import NavigationPlayer from '../../components/NavigarionPlayer';
import Picture from '../../components/Picture';
import styles from './Player.module.css';


function Player() {
    const {id}=useParams();
    const [bookInfo, setBookInfo]=useState(null);
useEffect(()=>{
    getBookById(setBookInfo,id)
},[])

    return (

        <Background typeBackground="player">
            <NavigationPlayer title={bookInfo?.title?bookInfo.title:null}/>
            <Picture type='bookCoverPlayer' img={bookInfo?.picture?bookInfo.picture.url:null} />

                <div className={styles.titlePlayed}>{bookInfo&&bookInfo.title}</div>
                <div className={styles.authorPlayer}>{bookInfo&&bookInfo.author}</div>

                <figure  className={styles.playerButtons} >
                    <audio  
                        controls
                        src={bookInfo&&bookInfo.soundBite.url}>
                            <a href={bookInfo&&bookInfo.soundBite.url}>
                                Download audio
                            </a>
                    </audio>
                </figure>
                
        <HomeInteractor/>
        </Background>
    );
}

export default Player;
