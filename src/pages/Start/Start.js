import Background from '../../components/Background';
import {useState} from 'react';
import { useParams } from 'react-router-dom';
import Carousel from '../../components/Carousel';
import Button from '../../components/Button';
import styles from './Start.module.css';

// start da idva от рутера и в зависимост на коя страница си да изписва на коя картинка си

function Start() {
    const {img}=useParams();
    const [count, setPicCount] = useState(1);
    const handleNext=()=>{
        if (count<3) {
            setPicCount(count+1);
        }
    }
    let splitImg=img.split(/-|\./)
   
    let picName='';
    splitImg.map((x,i)=>{
        if (i===1) {
            picName+=`-${count}`;
        } else if (i===2) {
            picName+=`.${x}`
        } else if (i===0) {
             picName+=x;
        }
    })
    const title=['Title One','Title Two','Title Three'];
    const description =['Ishmail is the name dolor sit amet consectetur adipisicing elit','Two is Lorem ipsum dolor sit amet consectetur adipisicing elit','My Dear John is Lorem ipsum dolor sit amet consectetur adipisicing elit']
    
    return (
        <div className='startPage'>

        <Background img={picName} >
        <h1 className={styles.titleStart}>{title[count-1]}</h1> 
            <h2>{description[count-1]}</h2>    
        </Background>
        <Button  onClick={handleNext} count={count}/>
       <Carousel count={count} />
        </div>
    );
}

export default Start;